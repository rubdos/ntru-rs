#[allow(non_snake_case)]
#[derive(Clone,Copy)]
pub struct NtruParameters {
    pub p: u16,
    pub q: u16,
    pub q_base: u16,
    pub q_exp: u32,
    pub N: u16,
}

pub static DEBUG_PARAMS: NtruParameters = NtruParameters {
    p: 3,
    q: 32,
    q_base: 2,
    q_exp: 5,
    N: 11,
};
