use polynomial::*;

mod generator;
mod params;

pub use self::params::NtruParameters;

pub struct NtruPublicKey {
    h: Polynomial,
}

pub struct NtruPrivateKey {
    f: Polynomial,
    g: Polynomial,
    fp: Polynomial,
    fq: Polynomial,
    params: NtruParameters,
}

impl NtruPrivateKey {
    fn from_f_g_params(mut f: Polynomial,
                       g: Polynomial,
                       params: NtruParameters) -> NtruPrivateKey {
        f.set_modulus(params.p, 1);
        let fp = f.invert();
        f.set_modulus(params.q_base, params.q_exp);
        let fq = f.invert();
        NtruPrivateKey {
            f: f,
            g: g,
            fp: fp,
            fq: fq,
            params: params,
        }
    }
    fn get_public_key(&self) -> NtruPublicKey {
        let mut h = self.params.p as i16 * (&self.fq * &self.g);
        h.reduce();
        NtruPublicKey {
            h: h
        }
    }
}

#[test]
fn calculate_public_key() {
    let f = Polynomial::new(11,
        vec![-1, 1, 1, 0, -1, 0, 1, 0, 0, 1, -1].into(),
        3, 1);
    let g = Polynomial::new(11,
        vec![-1, 0, 1, 1, 0, 1, 0, 0, -1, 0, -1].into(),
        3, 1);
    let prv = NtruPrivateKey::from_f_g_params(f, g, params::DEBUG_PARAMS);
    let mut pbl = prv.get_public_key();
    let NtruPublicKey {
        mut h
    } = pbl;
    h.normalize();

    let result = vec![8, 32-7, 22, 20, 12, 32-8, 15, 32-13, 12, 32-13, 16];
    assert_eq!(h.degree(), result.len() - 1, "pbl = {}", h);
    for (c, r) in result.iter().zip(h.coefficients.iter()) {
        assert_eq!(c, r);
    }
}
