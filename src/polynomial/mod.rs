use std;
use std::collections::VecDeque;

mod ops;
pub use self::ops::*;

// A polynomial in Z/(X^N-1)
#[allow(non_snake_case)]
pub struct Polynomial {
    N: usize,
    pub coefficients: VecDeque<i16>,
    modulus: i16,
    modulus_base: u16,
    modulus_exponent: u32,
}

trait Invertible {
    fn invert_m(&self, m: usize) -> Self;
}

impl Invertible for i16 {
    fn invert_m(&self, m: usize) -> i16 {
        // TODO: this is slow and inefficient, esp. for m large.
        let mut c = 1;
        for _ in 0..(m as usize) {
            c *= *self;
            c %= m as i16;
        }
        c
    }
}

impl std::fmt::Display for Polynomial {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        if self.coefficients.is_empty() {
            try!(write!(f, "0"));
            return Ok(());
        }
        let mut printed = false;
        for (c, i) in self.coefficients.iter().zip(0..) {
            if *c == 0 {
                continue;
            }
            if printed && *c > 0 {
                try!(write!(f, " +"));
            }
            if *c != 1 {
                if *c == -1 && i != 0 {
                    try!(write!(f, " -"));
                } else {
                    try!(write!(f, " {}", c));
                }
            } else if self.coefficients.len() == 1 {
                try!(write!(f, " {}", c));
            }
            if i > 0 {
                try!(write!(f, "X"));
            }
            if i > 1 {
                try!(write!(f, "^{}", i));
            }
            printed = true;
        }
        Ok(())
    }
}

impl Polynomial {
    pub fn new(degree: usize, coefficients: VecDeque<i16>, modulus_base: u16, modulus_exponent: u32) -> Polynomial {
        Polynomial {
            N: degree,
            coefficients: coefficients,
            modulus_base: modulus_base,
            modulus: modulus_base.pow(modulus_exponent) as i16,
            modulus_exponent: modulus_exponent,
        }
    }

    fn empty(degree: usize, modulus_base: u16, modulus_exponent: u32) -> Polynomial {
        Self::new(degree, VecDeque::with_capacity(degree as usize + 1),
                  modulus_base, modulus_exponent)
    }

    pub fn set_modulus(&mut self, base: u16, exp: u32) {
        self.modulus_base = base;
        self.modulus_exponent = exp;
        self.modulus = base.pow(exp as u32) as i16;
    }

    pub fn degree(&self) -> usize {
        if self.coefficients.len() == 0 {
            0
        } else {
            self.coefficients.len() - 1
        }
    }

    fn remove_zeros(&mut self) {
        for c in self.coefficients.iter_mut() {
            *c %= self.modulus;
        }
        while let Some(&0) = self.coefficients.back() {
            self.coefficients.pop_back();
        }
    }

    pub fn reduce(&mut self) {
        self.remove_zeros();
        while self.N <= self.degree() {
            let a = *self.coefficients.back().unwrap();

            let m = self.degree() - self.N;
            let mut g = Polynomial::new(
                m + self.N,
                vec![0; self.degree() + 1].into(),
                self.modulus_base,
                self.modulus_exponent
            );
            g.coefficients[m] = -a;
            g.coefficients[self.degree()] = a;

            *self -= &g;
            self.remove_zeros();
        }
    }

    pub fn invert(&self) -> Polynomial {
        if self.modulus_exponent == 1 {
            self.invert_prime()
        } else {
            let mut a = Polynomial::new(
                self.N,
                self.coefficients.clone(),
                self.modulus_base,
                self.modulus_exponent
            );
            let mut b = a.invert_prime();
            b.set_modulus(self.modulus_base, self.modulus_exponent);
            println!("b = {} (base inversion)", b);
            let mut q = self.modulus_base;
            while q < self.modulus as u16 {
                q *= q;
                //b.modulus = q as i16;
                //a.modulus = q as i16;
                b = &b * (2-&a*&b);
                for c in b.coefficients.iter_mut() {
                    *c %= self.modulus as i16;
                }
                println!("New b={} = b*(2-a*b)", b);
            }
            b.set_modulus(self.modulus_base, self.modulus_exponent);
            b.reduce();
            b
        }
    }

    fn invert_prime(&self) -> Polynomial {
        let mut k: i16 = 0;
        let mut b = Polynomial::new(
            self.N,
            vec![1].into(),
            self.modulus_base,
            1
        );
        let mut c = Polynomial::empty(self.N,
                                      self.modulus_base,
                                      1);
        let mut f = Polynomial::new(
            self.N,
            self.coefficients.clone(),
            self.modulus_base,
            1
        );
        let mut g = Polynomial::empty(self.N + 1,
                                      self.modulus_base,
                                      1);
        g.coefficients.push_back(-1);
        for _ in 1..self.N {
            g.coefficients.push_back(0);
        }
        g.coefficients.push_back(1);
        println!("deg(f) = {} deg(g) = {}", f.degree(), g.degree());
        let mut i = 0;
        f.reduce();
        loop {
            println!("f={}", f);
            while f.coefficients[0] == 0 {
                f.coefficients.pop_front(); // f(X) <- f(X)/X
                c.coefficients.push_front(0);
                k += 1;
                println!("f(X) := f(X)/X = {}, k={}", f, k);
            }
            if f.degree() == 0 {
                // Solution found: X^(N-k)*b(X)*f_0^-1 mod (X^N-1)

                b *= f.coefficients[0].invert_m(self.modulus_base as usize);

                let mut res = Polynomial::empty(self.N,
                                                self.modulus_base,
                                                self.modulus_exponent);
                let zeros = {
                    let mut zeros = (self.N as i16 - k) % self.N as i16;
                    if zeros < 0 {
                        zeros += self.N as i16;
                    }
                    zeros
                };
                for _ in 0..zeros {
                    res.coefficients.push_back(0);
                }
                res.coefficients.push_back(1);
                println!("res = {}, N-k={}, k={}", res, zeros, k);
                res *= &b;
                res.reduce();
                println!("---");
                return res;
            }
            if f.degree() < g.degree() {
                std::mem::swap(&mut f, &mut g);
                std::mem::swap(&mut b, &mut c);
            }
            let u = f.coefficients[0]*g.coefficients[0].invert_m(g.modulus_base as usize) % f.modulus_base as i16;
            f -= u*&g;
            b -= u*&c;
            println!("f={}",f);
            println!("b={}",b);
            f.remove_zeros();
            b.remove_zeros();
            if i > 20 {
                panic!("infinite loop");
            }
            i += 1;
        }
    }

    pub fn normalize(&mut self) {
        for c in self.coefficients.iter_mut() {
            *c %= self.modulus;
            if *c < 0 {
                *c += self.modulus;
            }
        }
    }
}



#[test]
fn test_reduce() {
    let mut p1 = Polynomial::new(
        3,
        vec![2, 0, 0, 1].into(),
        3, 1
    );
    p1.reduce();
    assert_eq!(p1.degree(), 0);
    assert_eq!(p1.coefficients.len(), 0);

    let mut p1 = Polynomial::new(
        3,
        vec![0, 0, 0, 1].into(),
        3, 1
    );
    p1.reduce();
    assert_eq!(p1.degree(), 0);
    assert_eq!(p1.coefficients.len(), 1);
    assert_eq!(p1.coefficients[0], 1);

    let mut p1 = Polynomial::new(
        3,
        vec![0, 2, 0, 0, 1].into(),
        3, 1
    );
    p1.reduce();
    assert_eq!(p1.degree(), 0);
    assert_eq!(p1.coefficients.len(), 0);

    // More complex example from https://en.wikipedia.org/wiki/NTRUEncrypt
}

#[test]
fn test_simple_invert() {
    // inv(X) = X^10 <=> X^11 = 1 mod (X^11-1)
    let p1 = Polynomial::new(
        11,
        vec![0, 1].into(),
        3, 1
    );

    let result = vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
    let out = p1.invert();

    assert_eq!(result.len() - 1, out.degree(),
        "Degree of this inverted polynomial should be {}, but equals {}",
        result.len() - 1, out.degree());
    for i in 0..result.len() {
        assert_eq!(result[i], out.coefficients[i], "x^{}", i);
    }

    // Now the other way around
    let p1 = Polynomial::new(
        11,
        vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1].into(),
        3, 1
    );

    let result = vec![0, 1];
    let out = p1.invert();

    assert_eq!(result.len() - 1, out.degree(),
        "Degree of this inverted polynomial should be {}, but equals {}",
        result.len() - 1, out.degree());
    for i in 0..result.len() {
        assert_eq!(result[i], out.coefficients[i], "x^{}", i);
    }
}

#[test]
fn test_invert() {
    // Examples from https://en.wikipedia.org/wiki/NTRUEncrypt
    let p1 = Polynomial::new(
        11,
        vec![2, 1, 1, 0, 2, 0, 1, 0, 0, 1, 2].into(),
        3, 1
    );

    let result = vec![1, 2, 0, 2, 2, 1, 0, 2, 1, 2];
    let mut out = p1.invert();
    out.normalize();

    let mut one = &out * &p1;
    one.reduce();
    one.normalize();
    assert_eq!(one.coefficients.len(), 1, "{} should == 1", one);
    assert_eq!(one.coefficients[0], 1, "{} should == 1", one);

    assert_eq!(result.len() - 1, out.degree(),
        "Degree of this inverted polynomial should be {}, but equals {}",
        result.len() - 1, out.degree());
    for i in 0..result.len() {
        assert_eq!(result[i], out.coefficients[i], "{}", out);
    }
}

#[test]
fn test_obvious_invert() {
    // inv(X) = X^10 <=> X^11 = 1 mod (X^11-1)
    let mut p1 = Polynomial::new(
        11,
        vec![0, 1].into(),
        3, 1
    );

    let out = p1.invert();

    p1 *= &out;
    p1.reduce();

    assert_eq!(p1.degree(), 0,
        "Degree of this inverted polynomial should be {}, but equals {}",
        0, p1.degree());
    assert_eq!(p1.coefficients[0], 1);

    // Some other one
    let mut p1 = Polynomial::new(
        11,
        vec![0, 0, 2].into(),
        3, 1
    );

    let out = p1.invert();

    println!("p_1^-1 = {}", out);
    p1 *= &out;
    p1.reduce();

    assert!(p1.coefficients.len() > 0, "{}", p1);
    assert_eq!(p1.coefficients[0], 1, "{}", p1);

    // A more difficult one
    let p1 = Polynomial::new(
        11,
        vec![2, 1, 1, 0, 2, 0, 1, 0, 0, 1, 2].into(),
        3, 1
    );
    println!("{}", p1);
    let mut out = p1.invert();

    out *= &p1;
    out.reduce();
    assert_eq!(out.degree(), 0, "{} = a * a^-1 should equal to one.", out);
    assert_eq!(out.coefficients[0], 1);
}

#[test]
fn invert_powered_prime() {
    let mut f = Polynomial::new(
        11,
        vec![-1, 1, 1, 0, -1, 0, 1, 0, 0, 1, -1].into(),
        2, 5);
    let mut f_q = f.invert();
    f_q.normalize();

    let result = vec![5, 9, 6, 16, 4, 15, 16, 22, 20, 18, 30];

    let mut one = &f_q * &f;
    one.normalize();
    assert_eq!(one.modulus, 32);
    assert_eq!(one.modulus_base, 2);
    assert_eq!(one.modulus_exponent, 5);
    assert_eq!(f_q.modulus, 32);
    assert_eq!(f_q.modulus_base, 2);
    assert_eq!(f_q.modulus_exponent, 5);
    println!("one: {}, f={}, f_q={}", one, f, f_q);
    one.reduce();
    assert_eq!(one.coefficients.len(), 1, "{} should == 1", one);
    assert_eq!(one.coefficients[0], 1, "{} should == 1", one);

    assert_eq!(result.len() - 1, f_q.degree(),
        "Degree of this inverted polynomial should be {}, but equals {}",
        result.len() - 1, f_q.degree());
    for i in 0..result.len() {
        assert_eq!(result[i], f_q.coefficients[i], "{}", f_q);
    }
}
