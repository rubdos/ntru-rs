use std;
use std::collections::VecDeque;
use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

use super::Polynomial;

impl<'a> Add for &'a Polynomial {
    type Output = Polynomial;
    fn add(self, rhs: Self) -> Self::Output {
        let (smallest, biggest) = if rhs.coefficients.len() < self.coefficients.len() {
            (&rhs.coefficients, &self.coefficients)
        } else {
            (&self.coefficients, &rhs.coefficients)
        };

        let zero = 0i16;
        let zero_iterator = (0usize..).map(|_| &zero);

        Polynomial::new(
            self.N,
            smallest.iter()
                .chain(zero_iterator)
                .zip(biggest).map(|(x, y)| (x + y) % self.modulus).collect(),
            self.modulus_base,
            self.modulus_exponent
        )
    }
}

impl<'a> AddAssign<&'a Polynomial> for Polynomial {
    fn add_assign(&mut self, rhs: &'a Polynomial) {
        let m = self.modulus;
        self.coefficients.iter_mut().zip(rhs.coefficients.iter())
            .map(|(x, y)| {
                *x += *y;
                *x %= m;
            }).collect::<Vec<()>>();

        if rhs.coefficients.len() > self.coefficients.len() {
            let to_skip = self.coefficients.len();
            self.coefficients.extend(rhs.coefficients.iter().skip(to_skip));
        }
    }
}

impl Sub<Polynomial> for i16 {
    type Output = Polynomial;
    fn sub(self, mut rhs: Polynomial) -> Polynomial {
        for x in rhs.coefficients.iter_mut() {
            *x = -*x;
        }
        if rhs.coefficients.len() == 0 {
            rhs.coefficients.push_back(self);
        } else {
            rhs.coefficients[0] += self;
        }
        rhs
    }
}

impl SubAssign<Polynomial> for Polynomial {
    fn sub_assign(&mut self, rhs: Polynomial) {
        *self -= &rhs;
    }
}

impl <'a> SubAssign<&'a Polynomial> for Polynomial {
    fn sub_assign(&mut self, rhs: &'a Polynomial) {
        let m = self.modulus;
        self.coefficients.iter_mut().zip(rhs.coefficients.iter())
            .map(|(x, y)| {
                *x -= *y;
                *x %= m;
            }).collect::<Vec<()>>();

        if rhs.coefficients.len() > self.coefficients.len() {
            let to_skip = self.coefficients.len();
            self.coefficients.extend(rhs.coefficients.iter().skip(to_skip).map(|x| -x));
        }
    }
}

impl<'a, 'b> Mul<&'b Polynomial> for &'a Polynomial {
    type Output = Polynomial;
    fn mul(self, rhs: &'b Polynomial) -> Self::Output {
        let mut out: VecDeque<i16> = vec![0; self.degree() + rhs.degree() + 1].into();
        for i in 0..self.coefficients.len() {
            for j in 0..rhs.coefficients.len() {
                out[i+j] += self.coefficients[i] * rhs.coefficients[j];
            }
        }
        for e in out.iter_mut() {
            *e %= self.modulus;
        }
        Polynomial::new(
            self.N,
            out,
            self.modulus_base,
            self.modulus_exponent
        )
    }
}

impl<'a> Mul<Polynomial> for &'a Polynomial {
    type Output = Polynomial;
    fn mul(self, rhs: Polynomial) -> Self::Output {
        self * &rhs
    }
}

impl<'a> MulAssign<&'a Polynomial> for Polynomial {
    fn mul_assign(&mut self, rhs: &'a Polynomial) {
        let mut out: VecDeque<_> = vec![0; self.degree() + rhs.degree() + 1].into();
        for i in 0..self.coefficients.len() {
            for j in 0..rhs.coefficients.len() {
                out[i+j] += self.coefficients[i] * rhs.coefficients[j];
            }
        }
        self.coefficients = out;
    }
}

impl Mul<Polynomial> for i16 {
    type Output = Polynomial;
    fn mul(self, mut rhs: Polynomial) -> Polynomial {
        for c in rhs.coefficients.iter_mut() {
            *c *= self;
            *c %= rhs.modulus;
        }
        rhs
    }
}

impl<'a> Mul<&'a Polynomial> for i16 {
    type Output = Polynomial;
    fn mul(self, rhs: &'a Polynomial) -> Polynomial {
        Polynomial::new(
            rhs.N,
            rhs.coefficients.iter().map(|c| {
                self * *c % rhs.modulus
            }).collect(),
            rhs.modulus_base,
            rhs.modulus_exponent
        )
    }
}

impl MulAssign<i16> for Polynomial {
    fn mul_assign(&mut self, rhs: i16) {
        for el in self.coefficients.iter_mut() {
            *el *= rhs;
            *el %= self.modulus;
        }
    }
}

#[test]
fn simple_add() {
    let p1 = Polynomial::new(
        11,
        vec![1, 2, 3, 4].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![-1, -2, -3, -4].into(),
        3, 1
    );
    let out = &p1 + &p2;
    for i in 0..4 {
        assert_eq!(p1.coefficients[i] + p2.coefficients[i], out.coefficients[i]);
    }

    let p1 = Polynomial::new(
        11,
        vec![0].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![0, 1].into(),
        3, 1
    );
    let out = &p1 + &p2;
    assert_eq!(out.coefficients[0], 0, "{} + {} = {}", p1, p2, out);
    assert_eq!(out.coefficients[1], 1, "{} + {} = {}", p1, p2, out);
}

#[test]
fn simple_add_assign() {
    let mut p1 = Polynomial::new(
        11,
        vec![1, 2, 3, 4].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![-1, -2, -3, -4].into(),
        3, 1
    );
    p1 += &p2;
    for i in 0..4 {
        assert_eq!(p1.coefficients[i], 0);
    }

    let mut p1 = Polynomial::new(
        11,
        vec![0].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![0, 1].into(),
        3, 1
    );
    p1 += &p2;
    assert_eq!(p1.coefficients[0], 0, "{}", p1);
    assert_eq!(p1.coefficients[1], 1, "{}", p1);
}

//#[test]
//fn simple_sub() {
//    let p1 = Polynomial::new(
//        11,
//        vec![0].into(),
//        3
//    );
//    let p2 = Polynomial::new(
//        11,
//        vec![0, 1].into(),
//        3
//    );
//    let out = &p1 - &p2;
//    assert_eq!(out.coefficients[0], 0, "{} - {} = {}", p1, p2, out);
//    assert_eq!(out.coefficients[1], -1, "{} - {} = {}", p1, p2, out);
//}

#[test]
fn simple_sub_assign() {
    let mut p1 = Polynomial::new(
        11,
        vec![1, 2, 3, 4].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![1, 2, 3, 4].into(),
        3, 1
    );
    p1 -= &p2;
    for i in 0..4 {
        assert_eq!(p1.coefficients[i], 0);
    }
    assert_eq!(p1.degree(), 3);

    let mut p1 = Polynomial::new(
        11,
        vec![0].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![0, 1].into(),
        3, 1
    );
    p1 -= &p2;
    assert_eq!(p1.coefficients[0], 0, "{}", p1);
    assert_eq!(p1.coefficients[1], -1, "{}", p1);
    assert_eq!(p1.degree(), 1);
}

#[test]
fn unequal_add() {
    let p1 = Polynomial::new(
        11,
        vec![1, 2, 3, 4, 1].into(),
        5, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![-1, -2, -3, -4].into(),
        5, 1
    );
    let out = &p1 + &p2;
    for i in 0..4 {
        assert_eq!(p1.coefficients[i] + p2.coefficients[i], out.coefficients[i]);
    }
    assert_eq!(out.coefficients[4], 1);
}

#[test]
fn unequal_add2() {
    let p1 = Polynomial::new(
        11,
        vec![-1, -2, -3, -4].into(),
        7, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![1, 2, 3, 4, 1].into(),
        7, 1
    );
    let out = &p1 + &p2;
    for i in 0..4 {
        assert_eq!(p1.coefficients[i] + p2.coefficients[i], out.coefficients[i]);
    }
    assert_eq!(out.coefficients[4], 1);
}

#[test]
fn test_mul() {
    let p1 = Polynomial::new(
        11,
        vec![6, -5, 1].into(),
        30, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![1, 5, 3].into(),
        30, 1
    );

    let result = vec![6, 25, -6, -10, 3];
    let mut out = &p1 * &p2;
    out.reduce();
    assert_eq!(out.coefficients.len(), 5);
    for i in 0..result.len() {
        assert_eq!(result[i], out.coefficients[i], "x^{}", i);
    }

    // Example from https://en.wikipedia.org/wiki/NTRUEncrypt
    let mut p1 = Polynomial::new(
        11,
        vec![2, 1, 1, 0, 2, 0, 1, 0, 0, 1, 2].into(),
        3, 1
    );
    let p2 = Polynomial::new(
        11,
        vec![1, 2, 0, 2, 2, 1, 0, 2, 1, 2].into(),
        3, 1
    );

    // Mul
    let mut res = &p1 * &p2;
    res.reduce();
    assert_eq!(res.degree(), 0, "{}", res);
    assert_eq!(res.coefficients[0], 1);

    // MulAssign
    p1 *= &p2;
    p1.reduce();
    assert_eq!(p1.degree(), 0, "{}", p1);
    assert_eq!(p1.coefficients[0], 1);
}
